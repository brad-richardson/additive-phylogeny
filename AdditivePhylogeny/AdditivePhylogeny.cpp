#include "AdditivePhylogeny.h"


AdditivePhylogeny::AdditivePhylogeny(std::vector<std::string> nodeNames, std::vector < std::vector<int> > distanceMatrix)
{
	this->nodeNames = nodeNames;
	this->distanceMatrix = distanceMatrix;
	g = additivePhylogeny(nodeNames, distanceMatrix);
}

Graph AdditivePhylogeny::additivePhylogeny(std::vector<std::string> nodeNames, std::vector<std::vector<int>> matrix) {
	Graph newGraph;
	if (matrix.size() == 2) {
		newGraph.addLeafNode(nodeNames[0]);
		newGraph.addLeafNode(nodeNames[1]);
		newGraph.addEdge(nodeNames[0], nodeNames[1], matrix[0][1]);
		return newGraph;
	}
	int delta = -1;
	if (!isDegenerate(matrix)) {
		delta = findDelta(matrix);
		for (int i = 0; i < matrix.size(); ++i) {
			for (int j = 0; j < matrix[i].size(); ++j) {
				if (i != j) {
					matrix[i][j] -= 2 * delta;
				}
			}
		}
	}
	else {
		delta = 0;
	}
	//Find a triple i, j, k in D such that...
	std::vector<int> degenerateTriple = findDegenerateTriple(matrix);
	int distance = matrix[degenerateTriple[I]][degenerateTriple[J]];

	//Remove jth row and jth column from D
	std::vector<std::vector<int>> newMatrix = removeRowColumn(matrix, degenerateTriple[J]);
	std::vector<std::string> newNodeNames = nodeNames; // needed because this contains the names
	newNodeNames.erase(newNodeNames.begin() + degenerateTriple[J]);

	// T <- additivePhylogeny(D)
	newGraph = additivePhylogeny(newNodeNames, newMatrix);

	std::cout << newGraph.toString();
	std::cout << "---------------------" << std::endl;

	// add a new vertex v to T at distance x from i to k
	std::string newInnerNodeName = newGraph.addInnerNode();

	// need to check if two nodes point directly to each other or if there is an intermediate
	// if there is an intermediate, add node between those two instead
	// the current implementation will not work when there is more than 1 midpoint node
	//if distance from i to j is less than i to inner node, insert here.
	//else if distance from i to j is greater than i to inner node, insert there
	std::string midpointNodeName = newGraph.findMidpoint(nodeNames[degenerateTriple[I]], nodeNames[degenerateTriple[J]]);
	if (distance < newGraph.findMidpointDistance(nodeNames[degenerateTriple[I]], nodeNames[degenerateTriple[J]])) {
		newGraph.removeEdge(nodeNames[degenerateTriple[I]], midpointNodeName);
		newGraph.addEdge(nodeNames[degenerateTriple[I]], newInnerNodeName, distance);
		newGraph.addEdge(newInnerNodeName, midpointNodeName, distance);
	}
	else {
		newGraph.removeEdge(midpointNodeName, nodeNames[degenerateTriple[K]]);
		newGraph.addEdge(midpointNodeName, newInnerNodeName, distance);
		newGraph.addEdge(newInnerNodeName, nodeNames[degenerateTriple[K]], distance);
	}

	//newGraph.addEdge(nodeNames[degenerateTriple[I]], newInnerNodeName, distance);
	//newGraph.addEdge(newInnerNodeName, nodeNames[degenerateTriple[K]], distance);
	//newGraph.removeEdge(nodeNames[degenerateTriple[I]], nodeNames[degenerateTriple[K]]);

	newGraph.addLeafNode(nodeNames[degenerateTriple[J]]);
	newGraph.addEdge(nodeNames[degenerateTriple[J]], newInnerNodeName, 0);

	//Check if matrix D is not additive
	// TODO
	if (!newGraph.isMatrixAdditive(nodeNames, matrix))
		std::cout << "Matrix D is not additive";

	newGraph.extendHangingEdgesByDelta(delta);

	return newGraph;
}

bool AdditivePhylogeny::isDegenerate(std::vector<std::vector<int>> matrix) {
	int size = matrix.size();

	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			for (int k = 0; k < size; k++){
				if (i != j && k != i && k != j && matrix[i][j] + matrix[j][k] == matrix[i][k])
					return true;
			}
		}
	}
	return false;
}

int AdditivePhylogeny::findDelta(std::vector<std::vector<int>> matrix) {
	int size = matrix.size();
	int min = std::numeric_limits<int>::max();
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			if (i != j && matrix[i][j] < min)
				min = matrix[i][j];
		}
	}
	return (min - 1) / 2;
}

std::vector<int> AdditivePhylogeny::findDegenerateTriple(std::vector<std::vector<int>> matrix) {
	int size = matrix.size();

	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			for (int k = 0; k < size; k++){
				if (i != j && k != i && k != j && matrix[i][j] + matrix[j][k] == matrix[i][k])
					return { i, j, k };
			}
		}
	}

	return { -1, -1, -1 };
}

std::vector<std::vector<int>> AdditivePhylogeny::removeRowColumn(std::vector<std::vector<int>> matrix, int j) {
	std::vector<std::vector<int>> newMatrix = matrix;

	newMatrix.erase(newMatrix.begin() + j);

	for (int i = 0; i < newMatrix.size(); ++i) {
		newMatrix[i].erase(newMatrix[i].begin() + j);
	}

	return newMatrix;
}