#include <iostream>
#include <vector>
#include <string>

#include "AdditivePhylogeny.h"

int main() {

	std::vector<std::string> nodeNames = { "A", "B", "C", "D" };

	std::vector<std::vector<int>> distanceMatrix = 
	{ { 0, 4, 10, 9 },
	{ 4, 0, 8, 7 },
	{ 10, 8, 0, 9 },
	{ 9, 7, 9, 0 }
	};

	AdditivePhylogeny additivePhylogeny(nodeNames, distanceMatrix);

	Graph computedGraph = additivePhylogeny.getGraph();

	std::cout << computedGraph.toString();

	//send to class to process data and return graph

	return 0;
}