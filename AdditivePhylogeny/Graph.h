#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include "GraphNode.h"

class Graph
{
private:
	std::vector<GraphNode> G;
	int leafNodeCt;
	int innerNodeCt;

public:
	Graph() {
		innerNodeCt = 0;
		leafNodeCt = 0;
	}
	Graph(std::vector<std::string>);

	void Graph::addLeafNode(std::string node);
	std::string addInnerNode();
	void addEdge(std::string h, std::string t, int ); // Add the edge to the link list
	void removeEdge(std::string h, std::string t); // Remove all edges with these strings

	//find distances instead maybe?
	int findMidpointDistance(std::string h, std::string t);
	std::string findMidpoint(std::string h, std::string t);

	void extendHangingEdgesByDelta(int delta);

	bool isLeafNode(std::string nodeName);

	bool isMatrixAdditive(std::vector < std::string> nodeNames, std::vector<std::vector<int>> distanceMatrix);

	std::string toString();
};

