#pragma once
#include "Graph.h"
#include <vector>
#include <string>

class AdditivePhylogeny
{
private:
	std::vector<std::string> nodeNames;
	std::vector < std::vector<int> > distanceMatrix;

	const int I = 0;
	const int J = 1;
	const int K = 2;

	Graph g;

public:
	AdditivePhylogeny(std::vector<std::string>, std::vector < std::vector<int> >);

	Graph getGraph() {
		return g;
	}

	Graph additivePhylogeny(std::vector<std::string> nodeNames, std::vector<std::vector<int>> matrix);

	bool isDegenerate(std::vector<std::vector<int>>);

	int findDelta(std::vector<std::vector<int>>);

	std::vector<int> findDegenerateTriple(std::vector<std::vector<int>> matrix);

	std::vector<std::vector<int>> removeRowColumn(std::vector<std::vector<int>> matrix, int j);
};

