#include "Graph.h"


Graph::Graph(std::vector<std::string> nodeNames)
{
	this->leafNodeCt = nodeNames.size();

	for (std::string name : nodeNames)
	{
		G.push_back(GraphNode(name));
	}

	innerNodeCt = 0;
}

void Graph::addLeafNode(std::string node) {
	G.push_back(GraphNode(node));
	leafNodeCt++;
}

std::string Graph::addInnerNode() {
	std::string nodeName = std::to_string(innerNodeCt);
	GraphNode newNode = GraphNode(nodeName);
	newNode.isLeafNode = false;
	G.push_back(newNode);
	innerNodeCt++;
	return nodeName;
}

void Graph::addEdge(std::string h, std::string t, int distance) 
{
	int hLocation, tLocation;

	for (int i = 0; i < G.size(); ++i) {
		if (G[i].name == h) {
			hLocation = i;
		}
		else if (G[i].name == t) {
			tLocation = i;
		}
	}

	// Clear leaf nodes of any other edges
	//if (G[hLocation].isLeafNode && G[hLocation].edgeNodes.size() == 1) {
	//	//G[hLocation].edgeNodes.clear();
	//	//G[hLocation].edgeNodes.clear();
	//	G[tLocation].addEdge(G[hLocation].edgeNodes[0].toNode, distance);
	//	
	//}
	//else if (G[tLocation].isLeafNode && G[tLocation].edgeNodes.size() == 1) {
	//	//G[tLocation].edgeNodes.clear();
	//	G[hLocation].addEdge(G[tLocation].edgeNodes[0].toNode, distance);
	//}
	//else {
		G.at(hLocation).addEdge(t, distance);
		G.at(tLocation).addEdge(h, distance);
	//}

	
}
void Graph::removeEdge(std::string h, std::string t)
{
	int hLocation, tLocation;

	for (int i = 0; i < G.size(); ++i) {
		if (G[i].name == h) {
			hLocation = i;
		}
		else if (G[i].name == t) {
			tLocation = i;
		}
	}

	G.at(hLocation).removeEdge(t);
	G.at(tLocation).removeEdge(h);
}

int Graph::findMidpointDistance(std::string h, std::string t) {
	int hLocation, tLocation;

	for (int i = 0; i < G.size(); ++i) {
		if (G[i].name == h) {
			hLocation = i;
		}
		else if (G[i].name == t) {
			tLocation = i;
		}
	}

	return G[hLocation].edgeNodes[0].distance;
}

std::string Graph::findMidpoint(std::string h, std::string t) {
	int hLocation, tLocation;

	for (int i = 0; i < G.size(); ++i) {
		if (G[i].name == h) {
			hLocation = i;
		}
		else if (G[i].name == t) {
			tLocation = i;
		}
	}

	return G[hLocation].edgeNodes[0].toNode;
}

void Graph::extendHangingEdgesByDelta(int delta) {

	for (int i = 0; i < G.size(); ++i) {
		// Node is leaf node, extend all edges by delta
		if (G[i].isLeafNode) {
			for (int j = 0; j < G[i].edgeNodes.size(); ++j) {
				G[i].edgeNodes[j].distance += delta;
			}
		}
		// Node is inner node, only extend edges that point to leaf nodes
		else {
			for (int j = 0; j < G[i].edgeNodes.size(); ++j) {
				if (isLeafNode(G[i].edgeNodes[j].toNode)) {
					G[i].edgeNodes[j].distance += delta;
				}
			}
		}
	}
}

bool Graph::isLeafNode(std::string nodeName) {
	
	for (int i = 0; i < G.size(); ++i) {
		if (G[i].name == nodeName && G[i].isLeafNode) {
			return true;
		}
	}

	return false;
}

bool Graph::isMatrixAdditive(std::vector<std::string> nodeNames, std::vector<std::vector<int>> distanceMatrix) {

	for (int i = 0; i < nodeNames.size(); ++i) {

	}
	return true;
}

std::string Graph::toString() {
	std::stringstream ss;
	for (int i = 0; i < G.size(); ++i) {
		ss << G[i].name << " -> ";
		for (int j = 0; j < G[i].edgeNodes.size(); ++j) {
			ss << G[i].edgeNodes[j].toNode << "(" << G[i].edgeNodes[j].distance << ") ";
		}
		ss << std::endl;
	}
	return ss.str();
}
