#pragma once

#include "EdgeNode.h"
#include <vector>

class GraphNode
{
public:
	//int nodeID;       // ID of node, from 0 to nodeCt-1
	std::string name;	 // Node name from A to Z
	std::vector<EdgeNode> edgeNodes; // adjacency list
	bool isLeafNode = true;

	GraphNode(std::string name)
	{
		this->name = name;
	}
	void addEdge(std::string toNode, int distance) {
		edgeNodes.push_back(EdgeNode(toNode, distance));
	}

	void removeEdge(std::string toNode) {
		int location = -1;
		for (int i = 0; i < edgeNodes.size(); i++) {
			if (edgeNodes[i].toNode == toNode) {
				location = i;
			}
		}
		if (location != -1) {
			edgeNodes.erase(edgeNodes.begin() + location);
		}
		else {
			//std::cout << "Problem";
		}
	}
};