#pragma once
#include <iostream>
#include <sstream>

class EdgeNode
{
public:
	std::string toNode;
	int distance;

	EdgeNode(std::string toNode, int distance){
		this->toNode = toNode;
		this->distance = distance;
	}
}; // EdgeNode 